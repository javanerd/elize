/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.agents;

import com.polarmsg.elize.base_classes.Agent;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;
import com.polarmsg.elize.properties.PropertyManager;

/**
 * StartupAgent Currently prints welcome message, can be scaled to do any
 * required task at startup.
 * 
 * @author fredladeroute
 *
 */
public class StartupAgent extends Agent {

    /**
     * True of the agents have started communication.
     */
    private static boolean      isStarted    = false;

    /**
     * Text to be printed to the GuiAgent on startup.
     */
    private static final String STARTUP_TEXT = PropertyManager
            .getStringProperty("brainbot.core.gui.startup.text",
                    "Hello I am Elize.");

    /**
     * Startup agent constructor.
     */
    public StartupAgent() {
        super("Startup Agent.", AgentId.STARTUP);
    }

    @Override
    public void activate() {
        // Currently unused.
    }

    @Override
    public final void update() {
        if (!StartupAgent.isStarted) {
            final ACLMessage msg = new ACLMessage(MessageType.SEND);
            msg.setActionID(ActionId.PRINT);
            msg.setSender(this.getId());
            msg.setReciever(AgentId.DISCORD);
            msg.setContent(StartupAgent.STARTUP_TEXT);
            msg.postMessage();
        }
        this.setStarted();
    }

    /**
     * Sets isStarted to true.
     */
    private void setStarted() {
        StartupAgent.isStarted = true;
    }

    /**
     * Return true if this agent is started.
     * 
     * @return true if this agent is started
     */
    public static boolean isStarted() {
        return StartupAgent.isStarted;
    }

    @Override
    public void dispose() {

    }

}
