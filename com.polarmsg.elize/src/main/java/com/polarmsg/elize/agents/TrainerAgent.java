/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.agents;

import java.io.File;

import org.apache.log4j.Logger;

import com.polarmsg.elize.base_classes.Agent;
import com.polarmsg.elize.classes.TrainDataFormatter;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;
import com.polarmsg.elize.properties.PropertyManager;

/**
 * TrainerAgent used to train a neural network.
 * 
 * @author fredladeroute
 *
 */
public class TrainerAgent extends Agent {

    /**
     * TrainDataFormatter instance used to format training data.
     */
    private TrainDataFormatter  tdf;

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = Logger.getLogger(TrainerAgent.class);

    /**
     * TrainerAgent constructor.
     */
    public TrainerAgent() {
        super("Trainer agent", AgentId.TRAINER);
    }

    @Override
    public final void activate() {
        final int numDatasets = PropertyManager
                .getIntegerProperty("ann.core.training.num_datasets", 10);
        final int numInputs = PropertyManager
                .getIntegerProperty("ann.core.training.num_inputs");
        final int numOutputs = PropertyManager
                .getIntegerProperty("ann.core.training.num_outputs");
        this.tdf = new TrainDataFormatter(numDatasets, numInputs, numOutputs,
                new File(PropertyManager
                        .getStringProperty("ann.core.training.datafile")));
    }

    @Override
    public final void update() {
        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
        if (this.checkBlackboard()) {
            msg = this.getNextMessage();
            if (msg.isEmpty()) {
                return;
            } else {
                switch (msg.getActionID()) {
                    case TRAIN:
                        this.train(msg.getContent());
                        break;
                    case NULL:
                        break;
                    case RAWDATA:
                        msg.setSender(this.getId());
                        msg.setReciever(AgentId.DISCORD);
                        msg.setActionID(ActionId.PRINT);
                        msg.postMessage();
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Perform training on the neural network.
     *
     * @param content
     *            the data in the form of a string to train the network with
     */
    private void train(final String content) {
        // TODO Complete TrainerAgent.train method line: 57

        // 1. Parse content and create training data.
        // 2, Add training data to the data stack.
        // 3. Check if the stack is at max size.
        // 4. If the stack is full then schedule a training with the
        // NeuralNetAgent
        // 5. If the stack is not full then
    }

    /**
     * Return a copy of the train data formatter.
     *
     * @return a copy of the train data formatter
     */
    public final TrainDataFormatter getTrainDataFormatter() {
        return this.tdf;
    }

    @Override
    public final void dispose() {
        // TODO If the program is shuting down, then serialize the train data
        // stack
        this.tdf = null;
        TrainerAgent.LOGGER.debug(this.getName() + " is disposing.");
    }

}
