/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.agents;

import java.util.Stack;

import javax.security.auth.login.LoginException;

import org.apache.log4j.Logger;

import com.polarmsg.elize.base_classes.Agent;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.EventListener;

/**
 * DiscordAgent responsible for sending message to the user.
 * 
 * @author fredladeroute
 *
 */
public class DiscordAgent extends Agent implements EventListener {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = Logger.getLogger(DiscordAgent.class);
    private Stack<String> messages;

    /**
     * GuiAgent constructor.
     */
    public DiscordAgent() {
        super("Discord Agent", AgentId.DISCORD);
    }

    @Override
    public final void activate() {
        JDA jda;
        try {
            jda = new JDABuilder(AccountType.BOT).setToken("MzUzNTU5NTU1ODQ3NzQ5NjMy.DI56zQ.b1iDlWlqzfrVCaXVWGt5NKucuPw").buildBlocking();
            jda.addEventListener(this);
        } catch (LoginException | IllegalArgumentException
                | InterruptedException | RateLimitedException e) {
            e.printStackTrace();
            LOGGER.error(e);
        }
        messages = new Stack<String>();
    }

    @Override
    public final void update() {
//        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
//        if (this.checkBlackboard()) {
//            msg = this.getNextMessage();
//            if (msg.isEmpty()) {
//                return;
//            } else {
//                switch (msg.getActionID()) {
//                    case PRINT:
//                        messages.add(msg.getContent());
//                        break;
////                    case INFORM:
////                        this.send(MessageType.RELAY, ActionId.INFORM,
////                                AgentId.DATA_STORAGE, msg.getContenttoken());
//                    default:
//                        break;
//                }
//            }
//        }
    }

    @Override
    public final void dispose() {
        DiscordAgent.LOGGER.debug("DiscordAgent is disposing.");
    }

    @Override
    public void onEvent(Event ev) {
        JDA jda = ev.getJDA();
 
        if (ev instanceof MessageReceivedEvent) {
            ACLMessage msg = new ACLMessage(MessageType.EMPTY);
            MessageReceivedEvent event = (MessageReceivedEvent) ev;
            User author = event.getAuthor();                //The user that sent the message
            Message message = event.getMessage();           //The message that was received.
            MessageChannel channel = event.getChannel();
            if (event.isFromType(ChannelType.TEXT)) {
                if (!author.isBot()) {
                    if (checkBlackboard()) {
                        msg = this.getNextMessage();
                        if (!msg.isEmpty()) {
                            switch (msg.getActionID()) {
                                case PRINT:
                                    channel.sendMessage(msg.getContent()).queue();
                                    break;
                                case INFORM:
                                    send(MessageType.RELAY, ActionId.INFORM,
                                            AgentId.DATA_STORAGE, message.getContent());
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }
    //TODO Draw out a graph for agent communication patterns. 
}
