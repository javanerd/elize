/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.agents;

import com.polarmsg.elize.base_classes.Agent;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;

/**
 * InquiryAgent ask the user for information.
 * 
 * @author fredladeroute
 *
 */
public class InquiryAgent extends Agent {

    /**
     * Inquiry string used for asking the user for an answer.
     */
    private static final String INQUIRY_RESPONCE = "What should I say?";

    /**
     * InquiryAgent constructor.
     */
    public InquiryAgent() {
        super("InquiryAgent", AgentId.INQUIRY);
    }

    @Override
    public void activate() {
    }

    @Override
    public final void update() {
        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
        if (this.checkBlackboard()) {
            msg = this.getNextMessage();
            if (msg.isEmpty()) {
                return;
            } else {
                switch (msg.getActionID()) {
                    case INQUIRE:
                        this.send(MessageType.SEND, ActionId.CHECK_CMD,
                                AgentId.COMMAND_AGENT, msg.getContent());
                        break;
                    case NO_ACTION:
                        this.send(MessageType.SEND, ActionId.PRINT, AgentId.DISCORD,
                                InquiryAgent.INQUIRY_RESPONCE);
                        // this.send(MessageType.SEND, ActionID.SPEAK,
                        // AgentID.TTS, InquiryAgent.INQUIRY_RESPONCE);
                        this.send(MessageType.SEND, ActionId.INFORM,
                                AgentId.DISCORD, msg.getContent());
                        break;
                    case INFORM:
                        this.send(MessageType.SEND, ActionId.PRINT, AgentId.DISCORD,
                                msg.getContent());
                        // this.send(MessageType.SEND, ActionID.SPEAK,
                        // AgentID.TTS, msg.getContent());
                        this.send(MessageType.SEND, ActionId.INFORM,
                                AgentId.DISCORD, msg.getContent());
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public void dispose() {
    }

}
