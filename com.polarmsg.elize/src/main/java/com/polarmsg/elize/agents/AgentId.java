/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * <p>
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/

package com.polarmsg.elize.agents;

/**
 * Agent ID.
 *
 * @author Fred Laderoute
 */
public enum AgentId {

    /**
     * Trainer agent id.
     */
    TRAINER(1),

    /**
     * Gui agent id.
     */
    DISCORD(2),

    /**
     * Startup agent id.
     */
    STARTUP(3),

    /**
     * Neuralnet agent id.
     */
    NEURALNET(4),

    // /**
    // * Text to speech agent id.
    // */
    // TTS(5),

    /**
     * Inquiry agent id.
     */
    INQUIRY(5),

    /**
     * Reply agent id.
     */
    REPLY(6),

    /**
     * Data storage id.
     */
    DATA_STORAGE(7),

    /**
     * Command agent id.
     */
    COMMAND_AGENT(8),

    /**
     * Image capture agent id.
     */
    IMAGE_CAPTURE(9);

    /**
     * id instance variable.
     */
    private int id;

    /**
     * AgentID constructor.
     * 
     * @param idx
     *            the id to set for this AgentID
     */
    private AgentId(final int idx) {
        this.id = idx;
    }

    /**
     * Return the id of AgentID idx as an integer.
     * 
     * @param idx
     *            the AgentID to get the id of
     * @return id of AgentID idx as an integer
     */
    public static int getId(final AgentId idx) {
        return idx.id;
    }
}
