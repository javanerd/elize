/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.agents;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.polarmsg.elize.base_classes.Agent;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;
import com.polarmsg.elize.utils.DatabaseUtils;

/**
 * ReplyAgent this agent is responsible for formulating a reply to the user.
 * 
 * @author fredladeroute
 *
 */
public class ReplyAgent extends Agent {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = Logger.getLogger(ReplyAgent.class);

    /**
     * ReplyAgent constructor.
     */
    public ReplyAgent() {
        super("ReplyAgent", AgentId.REPLY);
    }

    @Override
    public void activate() {
    }

    @Override
    public final void update() {

        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
        if (this.checkBlackboard()) {
            msg = this.getNextMessage();
            if (msg.isEmpty()) {
                return;
            } else {
                switch (msg.getActionID()) {
                    case REPLY:
                        String reply = "";
                        try {
                            reply = this.getReply(msg.getContent());
                        } catch (final SQLException e) {
                            ReplyAgent.LOGGER.warn(e);
                        }
                        if ((reply == null) || (reply.length() == 0)) {
                            this.send(MessageType.SEND, ActionId.INQUIRE,
                                    AgentId.INQUIRY, msg.getContent());
                        } else {
                            this.send(MessageType.SEND, ActionId.PRINT,
                                    AgentId.DISCORD, reply);
                            // this.send(MessageType.SEND, ActionID.SPEAK,
                            // AgentID.TTS, reply);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Get a reply from the reply table on the database given an input string
     * the method searches for that string in the database and returns a random
     * reply from that input's list of possible replies.
     * 
     * @param text
     *            the text to get a reply for.
     * @return a reply gathered from the database.
     * @throws SQLException
     *             if there is an error querying the database
     */
    private synchronized String getReply(final String text)
            throws SQLException {
        return DatabaseUtils.getInstance()
                .executeQuery("SELECT r.sentence " + "FROM ReplyMessages "
                        + "AS R JOIN UserMessages AS U "
                        + "ON R.UserMessages_idUserMessages = "
                        + "U.idUserMessages WHERE U.sentence = \"" + text
                        + "\" COLLATE NOCASE ORDER BY" + " RANDOM() LIMIT 1;")
                .getString(1);
    }

    @Override
    public void dispose() {
    }

}
