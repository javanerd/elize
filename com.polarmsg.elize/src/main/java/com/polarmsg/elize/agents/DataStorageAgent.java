/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.agents;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.polarmsg.elize.base_classes.Agent;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;
import com.polarmsg.elize.properties.PropertyManager;
import com.polarmsg.elize.utils.DatabaseUtils;

/**
 *
 * @author fredladeroute
 *
 */
public class DataStorageAgent extends Agent {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER       = Logger
            .getLogger(DataStorageAgent.class);

    /**
     * Regular expression to sanitize input strings.
     */
    private static final String REGEX        = PropertyManager
            .getStringProperty("brainbot.core.database.regex",
                    "[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)"
                            + "[^.!?]*)*[.!?]?['\"]?(?=\\s|$)");

    /**
     * Whether or not we need to store the current input vector as reply or not.
     */
    private boolean             storeAsReply = false;

    /**
     * Used locally to store data in database at correct index.
     */
    private int                 id;

    /**
     *
     */
    public DataStorageAgent() {
        super("DataStorageAgent", AgentId.DATA_STORAGE);
    }

    @Override
    public void activate() {
    }

    @Override
    public final void update() {
        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
        if (this.checkBlackboard()) {
            msg = this.getNextMessage();
            if (msg.isEmpty()) {
                return;
            } else {
                switch (msg.getActionID()) {
                    case RAWDATA:
                        if (this.storeAsReply) {
                            this.addReplyMessage(this.id, msg.getContent());
                            this.addUserMessage(msg.getContent());
                            this.storeAsReply = false;
                        } else {
                            this.addUserMessage(msg.getContent());
                            //DatabaseUtils.getInstance().dispose();
                        }

                        this.send(MessageType.SEND, ActionId.PRINT, AgentId.DISCORD,
                                msg.getContent());
                        this.send(MessageType.REPLY, ActionId.REPLY,
                                AgentId.REPLY, msg.getContent());

                        break;
                    case INFORM:
                        if (msg.getMessageType() == MessageType.RELAY) {
                            this.storeAsReply = true;
                                this.id = this.getUserMessageId(msg.getContent());
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Add a reply message to the database.
     * 
     * @param idx
     *            the index of the reply message
     * @param content
     *            the reply message to be added to the database
     */
    private void addReplyMessage(final int idx,
            final String content) {
        if (content.matches(DataStorageAgent.REGEX)) {
            DatabaseUtils.getInstance()
                    .executeUpdate("INSERT OR IGNORE INTO ReplyMessages "
                            + "VALUES (NULL,\"" + content + "\",\"" + idx
                            + "\");");
        } else {
            DataStorageAgent.LOGGER.debug("Error adding string to database,"
                    + " did not match regex.");
        }
    }

    /**
     * Get a reply index for a given user message.
     *
     * @param content
     *            the input message to get a reply for
     *
     * @return the index of the content string in the database or 0 if the
     *         string is not found.
     *
     */
    private int getUserMessageId(final String content) {
        int result = 0;
        final ResultSet rs = DatabaseUtils.getInstance()
                .executeQuery("SELECT * FROM UserMessages "
                        + "WHERE sentence = \"" + content
                        + "\" COLLATE NOCASE ORDER BY RANDOM() LIMIT 1;");
        if (rs == null) {
            return 0;
        } else {
            try {
                if (rs.next()) {
                    result = rs.getInt(1);
                }
            } catch (SQLException e) {
                DataStorageAgent.LOGGER.error(e);
            }
        }
        return result;
    }

    /**
     * Add a user message to the database.
     * 
     * @param userMessage
     *            the message to be added to the database the message must pass
     *            the <code>DataStorageAgent.REGEX</code> string before it is
     *            added to the database.
     */
    private void addUserMessage(final String userMessage) {
        if (userMessage.matches(DataStorageAgent.REGEX)) {
            DatabaseUtils.getInstance().executeUpdate(
                    "INSERT OR IGNORE INTO UserMessages(rowid,sentence) "
                            + "VALUES (NULL,\"" + userMessage + "\");");
        } else {
            DataStorageAgent.LOGGER.debug(
                    "Error adding string to database, did not match regex.");
        }

    }

    @Override
    public void dispose() {
    }
}
