/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 *<p>
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/

package com.polarmsg.elize.agents;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.polarmsg.elize.base_classes.Agent;
import com.polarmsg.elize.main.Main;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;

/**
 * Command Agent responsible for parsing and executing user commands.
 *
 * @author fredladeroute
 *
 */
public class CommandAgent extends Agent {

    /**
     * Current logger instance.
     */
    private static final Logger     LOGGER             = Logger
            .getLogger(CommandAgent.class);

    /**
     * Array list of locations.
     */
    private final ArrayList<String> locations          = new ArrayList<String>();

    /**
     * Array list of time tokens.
     */
    private final ArrayList<String> timeRecognizers    = new ArrayList<String>();

    /**
     * Array list of date tokens.
     */
    private final ArrayList<String> dateRecognizers    = new ArrayList<String>();

    /**
     * Array list of weather tokens.
     */
    private final ArrayList<String> weatherRecognizers = new ArrayList<String>();

    /**
     * Array list of ip tokens.
     */
    private final ArrayList<String> ipRecognizers      = new ArrayList<String>();

    /**
     * Array list of time zone tokens.
     */
    private final ArrayList<String> timezones          = new ArrayList<String>();

    /**
     * Array list of operational command tokens.
     */
    private final ArrayList<String> exitCommands       = new ArrayList<String>();

    /**
     * Response string to be sent back to calling agent.
     */
    private String                  response;

    /**
     * CommandAgent ctor, makes call to super.
     */
    public CommandAgent() {
        super("CommandAgent", AgentId.COMMAND_AGENT);
    }

    @Override
    public final void activate() {

        CommandAgent.LOGGER.debug("Adding operational commands.");
        this.addOperations();
        this.addTimeRecognizers();
        this.addDateRecognizers();
        this.addWeatherRecognizers();
        this.addIPRecognizers();
        this.addTimeZones();

        CommandAgent.LOGGER.debug("Adding timezones.");

    }

    /**
     * Add time zone tokens.
     */
    private void addTimeZones() {
        this.addZoneEtc();
        this.addZoneAmerica();
        this.addZonePacific();
        this.addZoneUS();
        this.addZoneCanada();
        this.addZoneEurope();
        this.addZoneAsia();
        this.addZoneAfrica();
        this.addZoneSystem();
        this.addZoneMisc();
    }

    /**
     * Add miscellaneous time zone tokens.
     */
    private void addZoneMisc() {
        this.timezones.add("MIT");
        this.timezones.add("HST");
        this.timezones.add("AST");
        this.timezones.add("Mexico/BajaNorte");
        this.timezones.add("PST");
        this.timezones.add("PST8PDT");
        this.timezones.add("MST");
        this.timezones.add("MST7MDT");
        this.timezones.add("Mexico/BajaSur");
        this.timezones.add("Navajo");
        this.timezones.add("PNT");
        this.timezones.add("CST");
        this.timezones.add("CST6CDT");
        this.timezones.add("Chile/EasterIsland");
        this.timezones.add("Mexico/General");
        this.timezones.add("Brazil/Acre");
        this.timezones.add("Cuba");
        this.timezones.add("EST");
        this.timezones.add("EST5EDT");
        this.timezones.add("IET");
        this.timezones.add("Jamaica");
        this.timezones.add("Antarctica/Palmer");
        this.timezones.add("Atlantic/Bermuda");
        this.timezones.add("Atlantic/Stanley");
        this.timezones.add("Brazil/West");
        this.timezones.add("Chile/Continental");
        this.timezones.add("PRT");
        this.timezones.add("CNT");
        this.timezones.add("AGT");
        this.timezones.add("Antarctica/Rothera");
        this.timezones.add("BET");
        this.timezones.add("Brazil/East");
        this.timezones.add("Atlantic/South_Georgia");
        this.timezones.add("Brazil/DeNoronha");
        this.timezones.add("Atlantic/Azores");
        this.timezones.add("Atlantic/Cape_Verde");
        this.timezones.add("Atlantic/Canary");
        this.timezones.add("Atlantic/Faeroe");
        this.timezones.add("Atlantic/Madeira");
        this.timezones.add("Atlantic/Reykjavik");
        this.timezones.add("Atlantic/St_Helena");
        this.timezones.add("Eire");
        this.timezones.add("GB");
        this.timezones.add("GB-Eire");
        this.timezones.add("GMT");
        this.timezones.add("GMT0");
        this.timezones.add("Greenwich");
        this.timezones.add("Iceland");
        this.timezones.add("Portugal");
        this.timezones.add("UCT");
        this.timezones.add("UTC");
        this.timezones.add("Universal");
        this.timezones.add("WET");
        this.timezones.add("Zulu");
        this.timezones.add("Arctic/Longyearbyen");
        this.timezones.add("Atlantic/Jan_Mayen");
        this.timezones.add("CET");
        this.timezones.add("ECT");
        this.timezones.add("MET");
        this.timezones.add("Poland");
        this.timezones.add("ART");
        this.timezones.add("CAT");
        this.timezones.add("EET");
        this.timezones.add("Egypt");
        this.timezones.add("Israel");
        this.timezones.add("Libya");
        this.timezones.add("Turkey");
        this.timezones.add("Antarctica/Syowa");
        this.timezones.add("EAT");
        this.timezones.add("Europe/Moscow");
        this.timezones.add("Indian/Antananarivo");
        this.timezones.add("Indian/Comoro");
        this.timezones.add("Indian/Mayotte");
        this.timezones.add("W-SU");
        this.timezones.add("Mideast/Riyadh87");
        this.timezones.add("Mideast/Riyadh88");
        this.timezones.add("Mideast/Riyadh89");
        this.timezones.add("Iran");
        this.timezones.add("Europe/Samara");
        this.timezones.add("Indian/Mahe");
        this.timezones.add("Indian/Mauritius");
        this.timezones.add("Indian/Reunion");
        this.timezones.add("NET");
        this.timezones.add("Indian/Kerguelen");
        this.timezones.add("Indian/Maldives");
        this.timezones.add("PLT");
        this.timezones.add("IST");
        this.timezones.add("Antarctica/Mawson");
        this.timezones.add("Antarctica/Vostok");
        this.timezones.add("BST");
        this.timezones.add("Indian/Chagos");
        this.timezones.add("Indian/Cocos");
        this.timezones.add("Antarctica/Davis");
        this.timezones.add("Indian/Christmas");
        this.timezones.add("VST");
        this.timezones.add("Antarctica/Casey");
        this.timezones.add("Australia/Perth");
        this.timezones.add("Australia/West");
        this.timezones.add("CTT");
        this.timezones.add("Hongkong");
        this.timezones.add("PRC");
        this.timezones.add("Singapore");
        this.timezones.add("JST");
        this.timezones.add("Japan");
        this.timezones.add("Pacific/Palau");
        this.timezones.add("ROK");
        this.timezones.add("ACT");
        this.timezones.add("Australia/Adelaide");
        this.timezones.add("Australia/Broken_Hill");
        this.timezones.add("Australia/Darwin");
        this.timezones.add("Australia/North");
        this.timezones.add("Australia/South");
        this.timezones.add("Australia/Yancowinna");
        this.timezones.add("AET");
        this.timezones.add("Antarctica/DumontDUrville");
        this.timezones.add("Australia/ACT");
        this.timezones.add("Australia/Brisbane");
        this.timezones.add("Australia/Canberra");
        this.timezones.add("Australia/Hobart");
        this.timezones.add("Australia/Lindeman");
        this.timezones.add("Australia/Melbourne");
        this.timezones.add("Australia/NSW");
        this.timezones.add("Australia/Queensland");
        this.timezones.add("Australia/Sydney");
        this.timezones.add("Australia/Tasmania");
        this.timezones.add("Australia/Victoria");
        this.timezones.add("Australia/LHI");
        this.timezones.add("Australia/Lord_Howe");
        this.timezones.add("Asia/Magadan");
        this.timezones.add("SST");
        this.timezones.add("Antarctica/McMurdo");
        this.timezones.add("Antarctica/South_Pole");
        this.timezones.add("Kwajalein");
        this.timezones.add("NST");
        this.timezones.add("NZ");
        this.timezones.add("NZ-CHAT");
    }

    /**
     * Add System time zone tokens.
     */
    private void addZoneSystem() {
        this.timezones.add("SystemV/HST10");
        this.timezones.add("SystemV/PST8");
        this.timezones.add("SystemV/PST8PDT");
        this.timezones.add("SystemV/CST6");
        this.timezones.add("SystemV/CST6CDT");
        this.timezones.add("SystemV/MST7");
        this.timezones.add("SystemV/MST7MDT");
        this.timezones.add("SystemV/YST9");
        this.timezones.add("SystemV/YST9YDT");
        this.timezones.add("SystemV/EST5");
        this.timezones.add("SystemV/EST5EDT");
        this.timezones.add("SystemV/AST4");
        this.timezones.add("SystemV/AST4ADT");
    }

    /**
     * Add African time zone tokens.
     */
    private void addZoneAfrica() {
        this.timezones.add("Africa/Addis_Ababa");
        this.timezones.add("Africa/Asmera");
        this.timezones.add("Africa/Dar_es_Salaam");
        this.timezones.add("Africa/Djibouti");
        this.timezones.add("Africa/Kampala");
        this.timezones.add("Africa/Khartoum");
        this.timezones.add("Africa/Mogadishu");
        this.timezones.add("Africa/Nairobi");
        this.timezones.add("Africa/Abidjan");
        this.timezones.add("Africa/Accra");
        this.timezones.add("Africa/Bamako");
        this.timezones.add("Africa/Banjul");
        this.timezones.add("Africa/Bissau");
        this.timezones.add("Africa/Casablanca");
        this.timezones.add("Africa/Conakry");
        this.timezones.add("Africa/Dakar");
        this.timezones.add("Africa/El_Aaiun");
        this.timezones.add("Africa/Freetown");
        this.timezones.add("Africa/Lome");
        this.timezones.add("Africa/Monrovia");
        this.timezones.add("Africa/Nouakchott");
        this.timezones.add("Africa/Ouagadougou");
        this.timezones.add("Africa/Sao_Tome");
        this.timezones.add("Africa/Timbuktu");
        this.timezones.add("Africa/Algiers");
        this.timezones.add("Africa/Bangui");
        this.timezones.add("Africa/Brazzaville");
        this.timezones.add("Africa/Ceuta");
        this.timezones.add("Africa/Douala");
        this.timezones.add("Africa/Kinshasa");
        this.timezones.add("Africa/Lagos");
        this.timezones.add("Africa/Libreville");
        this.timezones.add("Africa/Luanda");
        this.timezones.add("Africa/Malabo");
        this.timezones.add("Africa/Ndjamena");
        this.timezones.add("Africa/Niamey");
        this.timezones.add("Africa/Porto-Novo");
        this.timezones.add("Africa/Tunis");
        this.timezones.add("Africa/Windhoek");
        this.timezones.add("Africa/Blantyre");
        this.timezones.add("Africa/Bujumbura");
        this.timezones.add("Africa/Cairo");
        this.timezones.add("Africa/Gaborone");
        this.timezones.add("Africa/Harare");
        this.timezones.add("Africa/Johannesburg");
        this.timezones.add("Africa/Kigali");
        this.timezones.add("Africa/Lubumbashi");
        this.timezones.add("Africa/Lusaka");
        this.timezones.add("Africa/Maputo");
        this.timezones.add("Africa/Maseru");
        this.timezones.add("Africa/Mbabane");
        this.timezones.add("Africa/Tripoli");
    }

    /**
     * Add Asian time zone tokens.
     */
    private void addZoneAsia() {
        this.timezones.add("Asia/Tehran");
        this.timezones.add("Asia/Riyadh87");
        this.timezones.add("Asia/Riyadh88");
        this.timezones.add("Asia/Riyadh89");
        this.timezones.add("Asia/Rangoon");
        this.timezones.add("Asia/Calcutta");
        this.timezones.add("Asia/Katmandu");
        this.timezones.add("Asia/Bangkok");
        this.timezones.add("Asia/Hovd");
        this.timezones.add("Asia/Jakarta");
        this.timezones.add("Asia/Krasnoyarsk");
        this.timezones.add("Asia/Phnom_Penh");
        this.timezones.add("Asia/Pontianak");
        this.timezones.add("Asia/Saigon");
        this.timezones.add("Asia/Vientiane");
        this.timezones.add("Asia/Brunei");
        this.timezones.add("Asia/Chongqing");
        this.timezones.add("Asia/Chungking");
        this.timezones.add("Asia/Harbin");
        this.timezones.add("Asia/Hong_Kong");
        this.timezones.add("Asia/Irkutsk");
        this.timezones.add("Asia/Kashgar");
        this.timezones.add("Asia/Kuala_Lumpur");
        this.timezones.add("Asia/Kuching");
        this.timezones.add("Asia/Macao");
        this.timezones.add("Asia/Macau");
        this.timezones.add("Asia/Makassar");
        this.timezones.add("Asia/Manila");
        this.timezones.add("Asia/Shanghai");
        this.timezones.add("Asia/Singapore");
        this.timezones.add("Asia/Taipei");
        this.timezones.add("Asia/Ujung_Pandang");
        this.timezones.add("Asia/Ulaanbaatar");
        this.timezones.add("Asia/Ulan_Bator");
        this.timezones.add("Asia/Urumqi");
        this.timezones.add("Asia/Choibalsan");
        this.timezones.add("Asia/Dili");
        this.timezones.add("Asia/Jayapura");
        this.timezones.add("Asia/Pyongyang");
        this.timezones.add("Asia/Seoul");
        this.timezones.add("Asia/Tokyo");
        this.timezones.add("Asia/Yakutsk");
        this.timezones.add("Asia/Sakhalin");
        this.timezones.add("Asia/Vladivostok");
        this.timezones.add("Asia/Anadyr");
        this.timezones.add("Asia/Kamchatka");
        this.timezones.add("Asia/Almaty");
        this.timezones.add("Asia/Colombo");
        this.timezones.add("Asia/Dacca");
        this.timezones.add("Asia/Dhaka");
        this.timezones.add("Asia/Novosibirsk");
        this.timezones.add("Asia/Omsk");
        this.timezones.add("Asia/Qyzylorda");
        this.timezones.add("Asia/Thimbu");
        this.timezones.add("Asia/Thimphu");
        this.timezones.add("Asia/Kabul");
        this.timezones.add("Asia/Aqtobe");
        this.timezones.add("Asia/Ashgabat");
        this.timezones.add("Asia/Ashkhabad");
        this.timezones.add("Asia/Bishkek");
        this.timezones.add("Asia/Dushanbe");
        this.timezones.add("Asia/Karachi");
        this.timezones.add("Asia/Samarkand");
        this.timezones.add("Asia/Tashkent");
        this.timezones.add("Asia/Yekaterinburg");
        this.timezones.add("Asia/Aqtau");
        this.timezones.add("Asia/Baku");
        this.timezones.add("Asia/Dubai");
        this.timezones.add("Asia/Muscat");
        this.timezones.add("Asia/Oral");
        this.timezones.add("Asia/Tbilisi");
        this.timezones.add("Asia/Yerevan");
        this.timezones.add("Asia/Aden");
        this.timezones.add("Asia/Baghdad");
        this.timezones.add("Asia/Bahrain");
        this.timezones.add("Asia/Kuwait");
        this.timezones.add("Asia/Qatar");
        this.timezones.add("Asia/Riyadh");
        this.timezones.add("Asia/Amman");
        this.timezones.add("Asia/Beirut");
        this.timezones.add("Asia/Damascus");
        this.timezones.add("Asia/Gaza");
        this.timezones.add("Asia/Istanbul");
        this.timezones.add("Asia/Jerusalem");
        this.timezones.add("Asia/Nicosia");
        this.timezones.add("Asia/Tel_Aviv");
    }

    /**
     * Add European time zone tokens.
     */
    private void addZoneEurope() {
        this.timezones.add("Europe/Athens");
        this.timezones.add("Europe/Bucharest");
        this.timezones.add("Europe/Chisinau");
        this.timezones.add("Europe/Helsinki");
        this.timezones.add("Europe/Istanbul");
        this.timezones.add("Europe/Kaliningrad");
        this.timezones.add("Europe/Kiev");
        this.timezones.add("Europe/Minsk");
        this.timezones.add("Europe/Nicosia");
        this.timezones.add("Europe/Riga");
        this.timezones.add("Europe/Simferopol");
        this.timezones.add("Europe/Sofia");
        this.timezones.add("Europe/Tallinn");
        this.timezones.add("Europe/Tiraspol");
        this.timezones.add("Europe/Uzhgorod");
        this.timezones.add("Europe/Vilnius");
        this.timezones.add("Europe/Zaporozhye");
        this.timezones.add("Europe/Belfast");
        this.timezones.add("Europe/Dublin");
        this.timezones.add("Europe/Lisbon");
        this.timezones.add("Europe/London");
        this.timezones.add("Europe/Amsterdam");
        this.timezones.add("Europe/Andorra");
        this.timezones.add("Europe/Belgrade");
        this.timezones.add("Europe/Berlin");
        this.timezones.add("Europe/Bratislava");
        this.timezones.add("Europe/Brussels");
        this.timezones.add("Europe/Budapest");
        this.timezones.add("Europe/Copenhagen");
        this.timezones.add("Europe/Gibraltar");
        this.timezones.add("Europe/Ljubljana");
        this.timezones.add("Europe/Luxembourg");
        this.timezones.add("Europe/Madrid");
        this.timezones.add("Europe/Malta");
        this.timezones.add("Europe/Monaco");
        this.timezones.add("Europe/Oslo");
        this.timezones.add("Europe/Paris");
        this.timezones.add("Europe/Prague");
        this.timezones.add("Europe/Rome");
        this.timezones.add("Europe/San_Marino");
        this.timezones.add("Europe/Sarajevo");
        this.timezones.add("Europe/Skopje");
        this.timezones.add("Europe/Stockholm");
        this.timezones.add("Europe/Tirane");
        this.timezones.add("Europe/Vaduz");
        this.timezones.add("Europe/Vatican");
        this.timezones.add("Europe/Vienna");
        this.timezones.add("Europe/Warsaw");
        this.timezones.add("Europe/Zagreb");
        this.timezones.add("Europe/Zurich");
    }

    /**
     * Add Canadian time zone tokens.
     */
    private void addZoneCanada() {
        this.timezones.add("Canada/Newfoundland");
        this.timezones.add("Canada/Atlantic");
        this.timezones.add("Canada/Eastern");
        this.timezones.add("Canada/Central");
        this.timezones.add("Canada/Mountain");
        this.timezones.add("Canada/Pacific");
        this.timezones.add("Canada/Yukon");
        this.timezones.add("Canada/East-Saskatchewan");
        this.timezones.add("Canada/Saskatchewan");
    }

    /**
     * Add US time zone tokens.
     */
    private void addZoneUS() {
        this.timezones.add("US/East-Indiana");
        this.timezones.add("US/Eastern");
        this.timezones.add("US/Central");
        this.timezones.add("US/Arizona");
        this.timezones.add("US/Mountain");
        this.timezones.add("US/Pacific");
        this.timezones.add("US/Pacific-New");
        this.timezones.add("US/Alaska");
        this.timezones.add("US/Aleutian");
        this.timezones.add("US/Samoa");
        this.timezones.add("US/Hawaii");
        this.timezones.add("US/Indiana-Starke");
        this.timezones.add("US/Michigan");
    }

    /**
     * Add pacific time zones.
     */
    private void addZonePacific() {
        this.timezones.add("Pacific/Norfolk");
        this.timezones.add("Pacific/Easter");
        this.timezones.add("Pacific/Pitcairn");
        this.timezones.add("Pacific/Marquesas");
        this.timezones.add("Pacific/Gambier");
        this.timezones.add("Pacific/Galapagos");
        this.timezones.add("Pacific/Guam");
        this.timezones.add("Pacific/Port_Moresby");
        this.timezones.add("Pacific/Saipan");
        this.timezones.add("Pacific/Truk");
        this.timezones.add("Pacific/Yap");
        this.timezones.add("Pacific/Efate");
        this.timezones.add("Pacific/Guadalcanal");
        this.timezones.add("Pacific/Kosrae");
        this.timezones.add("Pacific/Noumea");
        this.timezones.add("Pacific/Ponape");
        this.timezones.add("Pacific/Chatham");
        this.timezones.add("Pacific/Enderbury");
        this.timezones.add("Pacific/Tongatapu");
        this.timezones.add("Pacific/Kiritimati");
        this.timezones.add("Pacific/Apia");
        this.timezones.add("Pacific/Midway");
        this.timezones.add("Pacific/Niue");
        this.timezones.add("Pacific/Pago_Pago");
        this.timezones.add("Pacific/Samoa");
        this.timezones.add("Pacific/Fakaofo");
        this.timezones.add("Pacific/Honolulu");
        this.timezones.add("Pacific/Johnston");
        this.timezones.add("Pacific/Rarotonga");
        this.timezones.add("Pacific/Tahiti");
        this.timezones.add("Pacific/Auckland");
        this.timezones.add("Pacific/Fiji");
        this.timezones.add("Pacific/Funafuti");
        this.timezones.add("Pacific/Kwajalein");
        this.timezones.add("Pacific/Majuro");
        this.timezones.add("Pacific/Nauru");
        this.timezones.add("Pacific/Tarawa");
        this.timezones.add("Pacific/Wake");
        this.timezones.add("Pacific/Wallis");
    }

    /**
     * Add American time zone tokens.
     */
    private void addZoneAmerica() {
        this.timezones.add("America/Adak");
        this.timezones.add("America/Atka");
        this.timezones.add("America/Anchorage");
        this.timezones.add("America/Juneau");
        this.timezones.add("America/Nome");
        this.timezones.add("America/Yakutat");
        this.timezones.add("America/Dawson");
        this.timezones.add("America/Ensenada");
        this.timezones.add("America/Los_Angeles");
        this.timezones.add("America/Tijuana");
        this.timezones.add("America/Vancouver");
        this.timezones.add("America/Whitehorse");
        this.timezones.add("America/Boise");
        this.timezones.add("America/Cambridge_Bay");
        this.timezones.add("America/Chihuahua");
        this.timezones.add("America/Dawson_Creek");
        this.timezones.add("America/Denver");
        this.timezones.add("America/Edmonton");
        this.timezones.add("America/Hermosillo");
        this.timezones.add("America/Inuvik");
        this.timezones.add("America/Mazatlan");
        this.timezones.add("America/Phoenix");
        this.timezones.add("America/Shiprock");
        this.timezones.add("America/Yellowknife");
        this.timezones.add("America/Belize");
        this.timezones.add("America/Cancun");
        this.timezones.add("America/Chicago");
        this.timezones.add("America/Costa_Rica");
        this.timezones.add("America/El_Salvador");
        this.timezones.add("America/Guatemala");
        this.timezones.add("America/Managua");
        this.timezones.add("America/Menominee");
        this.timezones.add("America/Merida");
        this.timezones.add("America/Mexico_City");
        this.timezones.add("America/Monterrey");
        this.timezones.add("America/North_Dakota/Center");
        this.timezones.add("America/Rainy_River");
        this.timezones.add("America/Rankin_Inlet");
        this.timezones.add("America/Regina");
        this.timezones.add("America/Swift_Current");
        this.timezones.add("America/Tegucigalpa");
        this.timezones.add("America/Winnipeg");
        this.timezones.add("America/Bogota");
        this.timezones.add("America/Cayman");
        this.timezones.add("America/Detroit");
        this.timezones.add("America/Eirunepe");
        this.timezones.add("America/Fort_Wayne");
        this.timezones.add("America/Grand_Turk");
        this.timezones.add("America/Guayaquil");
        this.timezones.add("America/Havana");
        this.timezones.add("America/Indiana/Indianapolis");
        this.timezones.add("America/Indiana/Knox");
        this.timezones.add("America/Indiana/Marengo");
        this.timezones.add("America/Indiana/Vevay");
        this.timezones.add("America/Indianapolis");
        this.timezones.add("America/Iqaluit");
        this.timezones.add("America/Jamaica");
        this.timezones.add("America/Kentucky/Louisville");
        this.timezones.add("America/Kentucky/Monticello");
        this.timezones.add("America/Knox_IN");
        this.timezones.add("America/Lima");
        this.timezones.add("America/Louisville");
        this.timezones.add("America/Montreal");
        this.timezones.add("America/Nassau");
        this.timezones.add("America/New_York");
        this.timezones.add("America/Nipigon");
        this.timezones.add("America/Panama");
        this.timezones.add("America/Pangnirtung");
        this.timezones.add("America/Port-au-Prince");
        this.timezones.add("America/Porto_Acre");
        this.timezones.add("America/Rio_Branco");
        this.timezones.add("America/Thunder_Bay");
        this.timezones.add("America/Toronto");
        this.timezones.add("America/Anguilla");
        this.timezones.add("America/Antigua");
        this.timezones.add("America/Aruba");
        this.timezones.add("America/Asuncion");
        this.timezones.add("America/Barbados");
        this.timezones.add("America/Boa_Vista");
        this.timezones.add("America/Campo_Grande");
        this.timezones.add("America/Caracas");
        this.timezones.add("America/Cuiaba");
        this.timezones.add("America/Curacao");
        this.timezones.add("America/Dominica");
        this.timezones.add("America/Glace_Bay");
        this.timezones.add("America/Goose_Bay");
        this.timezones.add("America/Grenada");
        this.timezones.add("America/Guadeloupe");
        this.timezones.add("America/Guyana");
        this.timezones.add("America/Halifax");
        this.timezones.add("America/La_Paz");
        this.timezones.add("America/Manaus");
        this.timezones.add("America/Martinique");
        this.timezones.add("America/Montserrat");
        this.timezones.add("America/Port_of_Spain");
        this.timezones.add("America/Porto_Velho");
        this.timezones.add("America/Puerto_Rico");
        this.timezones.add("America/Santiago");
        this.timezones.add("America/Santo_Domingo");
        this.timezones.add("America/St_Kitts");
        this.timezones.add("America/St_Lucia");
        this.timezones.add("America/St_Thomas");
        this.timezones.add("America/St_Vincent");
        this.timezones.add("America/Thule");
        this.timezones.add("America/Tortola");
        this.timezones.add("America/Virgin");
        this.timezones.add("America/St_Johns");
        this.timezones.add("America/Araguaina");
        this.timezones.add("America/Bahia");
        this.timezones.add("America/Belem");
        this.timezones.add("America/Buenos_Aires");
        this.timezones.add("America/Catamarca");
        this.timezones.add("America/Cayenne");
        this.timezones.add("America/Cordoba");
        this.timezones.add("America/Fortaleza");
        this.timezones.add("America/Godthab");
        this.timezones.add("America/Jujuy");
        this.timezones.add("America/Maceio");
        this.timezones.add("America/Mendoza");
        this.timezones.add("America/Miquelon");
        this.timezones.add("America/Montevideo");
        this.timezones.add("America/Paramaribo");
        this.timezones.add("America/Recife");
        this.timezones.add("America/Rosario");
        this.timezones.add("America/Sao_Paulo");
        this.timezones.add("America/Noronha");
        this.timezones.add("America/Scoresbysund");
        this.timezones.add("America/Danmarkshavn");

    }

    /**
     * Add ETC timezones.
     */
    private void addZoneEtc() {
        this.timezones.add("Etc/GMT+9");
        this.timezones.add("Etc/GMT+12");
        this.timezones.add("Etc/GMT+11");
        this.timezones.add("Etc/GMT+10");
        this.timezones.add("Etc/GMT-14");
        this.timezones.add("Etc/GMT+8");
        this.timezones.add("Etc/GMT+7");
        this.timezones.add("Etc/GMT+6");
        this.timezones.add("Etc/GMT+5");
        this.timezones.add("Etc/GMT+4");
        this.timezones.add("Etc/GMT+3");
        this.timezones.add("Etc/GMT+2");
        this.timezones.add("Etc/GMT+1");
        this.timezones.add("Etc/GMT");
        this.timezones.add("Etc/GMT+0");
        this.timezones.add("Etc/GMT-0");
        this.timezones.add("Etc/GMT0");
        this.timezones.add("Etc/Greenwich");
        this.timezones.add("Etc/UCT");
        this.timezones.add("Etc/UTC");
        this.timezones.add("Etc/Universal");
        this.timezones.add("Etc/Zulu");
        this.timezones.add("Etc/GMT-1");
        this.timezones.add("Etc/GMT-2");
        this.timezones.add("Etc/GMT-3");
        this.timezones.add("Etc/GMT-4");
        this.timezones.add("Etc/GMT-5");
        this.timezones.add("Etc/GMT-6");
        this.timezones.add("Etc/GMT-7");
        this.timezones.add("Etc/GMT-8");
        this.timezones.add("Etc/GMT-9");
        this.timezones.add("Etc/GMT-10");
        this.timezones.add("Etc/GMT-11");
        this.timezones.add("Etc/GMT-12");
        this.timezones.add("Etc/GMT-13");
    }

    /**
     * Add ip recognizer tokens.
     */
    private void addIPRecognizers() {
        this.ipRecognizers.add("what is my ip");
        this.ipRecognizers.add("ip address");
        this.ipRecognizers.add("know my ip");
    }

    /**
     * Add weather recognizer tokens.
     */
    private void addWeatherRecognizers() {
        this.weatherRecognizers.add("what is the temperature ");
        this.weatherRecognizers.add("how hot is it ");
        this.weatherRecognizers.add("how cold is it ");
        this.weatherRecognizers.add("cold today");
        this.weatherRecognizers.add("hot today");
        this.weatherRecognizers.add("the weather");
    }

    /**
     * Add date recognizer tokens.
     */
    private void addDateRecognizers() {
        this.dateRecognizers.add("the date is");
        this.dateRecognizers.add("day is it");
        this.dateRecognizers.add("the date");
        this.dateRecognizers.add("today's date");
        this.dateRecognizers.add("todays date");
    }

    /**
     * Add time recognizer tokens.
     */
    private void addTimeRecognizers() {
        this.timeRecognizers.add("time is it");
        this.timeRecognizers.add("what's the time");
        this.timeRecognizers.add("whats the time");
        this.timeRecognizers.add("know the time");
        this.timeRecognizers.add("what is the time");
        this.timeRecognizers.add("the current time");
    }

    /**
     * Add list of operational commands.
     */
    private void addOperations() {
        this.exitCommands.add("exit");
        this.exitCommands.add("quit");
        this.exitCommands.add("halt");
        this.exitCommands.add("bye");
        this.exitCommands.add("goodbye");
        this.exitCommands.add("stop");
        this.exitCommands.add("shut up");
        this.exitCommands.add("be quite");
        this.exitCommands.add("leave me alone");
        this.exitCommands.add("stop talking");
        this.exitCommands.add("stop talking to me");
        this.exitCommands.add("don't talk");
        this.exitCommands.add("i'm leaving");
    }

    @Override
    public final void update() {
        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
        if (this.checkBlackboard()) {
            msg = this.getNextMessage();
            if (msg.isEmpty()) {
                return;
            } else {
                switch (msg.getActionID()) {
                    case CHECK_CMD:
                        if (this.checkForCommands(msg.getContent())) {
                            System.out.println(this.response);
                            this.send(MessageType.REPLY, ActionId.INFORM,
                                    msg.getSender(), this.response);
                        } else {
                            this.send(MessageType.REPLY, ActionId.NO_ACTION,
                                    msg.getSender(), this.response);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Return the date for a given location.
     *
     * @param l
     *            the location to get the date for
     * @return the date at location l
     */
    private Date getDate(final String l) {
        final Calendar calendar = new GregorianCalendar();
        for (final String s : this.timezones) {
            if (s.replaceAll("_-/", " ").toLowerCase().contains(l)) {
                calendar.setTimeZone(TimeZone.getTimeZone(s));
            }
        }
        return calendar.getTime();
    }

    /**
     * Check to see if there is a command in the input string.
     *
     * @param content
     *            the input string to parse
     * @return if the input string contains any commands
     */
    private boolean checkForCommands(final String content) {

        for (final String s : this.exitCommands) {
            if (content.equalsIgnoreCase(s)) {
                Main.shutdown();
            }
        }

        for (final String s : this.timeRecognizers) {
            if (content.contains(s)) {
                for (final String l : this.locations) {
                    if (content.contains(" in " + l)) {
                        final String time = new SimpleDateFormat("h:mm a")
                                .format(this.getDate(l));
                        this.response = "The time in " + l + " is " + time;
                        return true;
                    }
                }

                final String time = new SimpleDateFormat("h:mm a")
                        .format(this.getDate("sudbury"));
                this.response = "The time is " + time;
                return true;
            }
        }

        for (final String s : this.dateRecognizers) {
            if (content.contains(s)) {
                final String date = new SimpleDateFormat("EEE, d MMM yyyy")
                        .format(this.getDate("sudbury"));
                this.response = "The date is " + date;
                return true;
            }
        }

        for (final String s : this.ipRecognizers) {
            if (content.contains(s)) {
                final StringBuilder ipvalue = new StringBuilder();
                try {
                    for (final Enumeration<NetworkInterface> en = NetworkInterface
                            .getNetworkInterfaces(); en.hasMoreElements();) {
                        final NetworkInterface intf = en.nextElement();
                        for (final Enumeration<InetAddress> enumIpAddr = intf
                                .getInetAddresses(); enumIpAddr
                                        .hasMoreElements();) {
                            final InetAddress inetAddress = enumIpAddr
                                    .nextElement();
                            if (!inetAddress.isLoopbackAddress()
                                    && !inetAddress.isLinkLocalAddress()
                                    && inetAddress.isSiteLocalAddress()) {
                                ipvalue.append(
                                        inetAddress.getHostAddress().toString()
                                                + "\n");
                            }

                        }
                    }
                } catch (final SocketException ex) {
                    ex.printStackTrace();
                }
                this.response = "The current internet protocol "
                        + "address of the computer I am running on is, "
                        + ipvalue.toString();

                return true;
            }
        }
        return false;
    }

    @Override
    public void dispose() {
    }

}
