/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.classes;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Stack;

import org.apache.log4j.Logger;

/**
 * Formats runtime data into file data.
 *
 * @author orthidax
 */
public class TrainDataFormatter {

    /**
     * Data file for the train data.
     */
    private final File          dataFile;

    /**
     * Number of datasets.
     */
    private int                 numDatasets;

    /**
     * Number of inputs to the neural network.
     */
    private int                 numInputs;

    /**
     * Number of outputs from the neural network.
     */
    private int                 numOutputs;

    /**
     * Stack containing input data.
     */
    private final Stack<Float>  inputDataStack  = new Stack<Float>();

    /**
     * Stack containing output data.
     */
    private final Stack<Float>  outputDataStack = new Stack<Float>();

    /**
     * Logger instance.
     */
    private static final Logger LOGGER          = Logger
            .getLogger(TrainDataFormatter.class);

    /**
     * @param pNumDatasets
     *            Total number of data points to train on the network.
     * @param pNumInputs
     *            The total number of inputs.
     * @param pNumOutputs
     *            The total number of expected output datapoints for training.
     * @param pDataFile
     *            The file to store and retrieve data from.
     */
    public TrainDataFormatter(final int pNumDatasets, final int pNumInputs,
            final int pNumOutputs, final File pDataFile) {
        this.setNumDatasets(pNumDatasets);
        this.setNumInputs(pNumInputs);
        this.setNumOutputs(pNumOutputs);
        this.dataFile = pDataFile;
    }

    /**
     * Add a row of data to the Training file.
     *
     * @param inData
     *            Data for input
     * @param outData
     *            Data for output
     * @return Success or fail (true/false)
     */
    public final boolean addRowData(final float[] inData,
            final float[] outData) {
        if (inData.length != this.getNumInputs()) {
            return false;
        }
        if (outData.length != this.getNumOutputs()) {
            return false;
        }

        FileWriter write = null;
        try {
            write = new FileWriter(this.dataFile, true);
        } catch (final IOException e) {
            TrainDataFormatter.LOGGER.error(e);
            e.printStackTrace();
        }
        final PrintWriter printWriter = new PrintWriter(write);
        for (int i = 0; i < (inData.length * 2); i++) {
            if (i == inData.length) {
                printWriter.println();
            } else if ((i % 0) == 1) {
                printWriter.print(inData[i]);
            } else {
                printWriter.print(outData[i]);
            }
        }
        printWriter.flush();
        printWriter.close();
        return true;
    }

    /**
     * Adds one value of input data to the internal data stack.
     *
     * @param value
     *            The value to be pushed onto the stack.
     */
    public final void addInputData(final float value) {
        if (this.inputDataStack.size() == this.getNumInputs()) {
            if (this.outputDataStack.size() == this.getNumOutputs()) {
                this.addRowDataFromStacks();
                return;
            }
            TrainDataFormatter.LOGGER.error(
                    "The input data vector is full. Possible off by 1 error. "
                            + "You should now start filling output vector.");
            return;
        }
        this.inputDataStack.push(value);
    }

    /**
     * Add output data to the stack.
     * 
     * @param value
     *            the value to be added to the output stack
     */
    public final void addOutputData(final float value) {
        if (this.outputDataStack.size() == this.getNumOutputs()) {
            if (this.inputDataStack.size() == this.getNumInputs()) {
                this.addRowDataFromStacks();
                return;
            }
            TrainDataFormatter.LOGGER.error("The output data vector "
                    + "is full. Possible off by 1 error. ");
            return;
        }
        this.outputDataStack.push(value);
    }

    /**
     * Get the data file reference.
     * 
     * @return the dataFile reference.
     */
    public final File getDataFile() {
        return this.dataFile;
    }

    /**
     * Get the number of data sets.
     * 
     * @return the num_datasets
     */
    public final int getNumDatasets() {
        return this.numDatasets;
    }

    /**
     * @param pNumDatasets
     *            the number of datasets to set
     */
    private void setNumDatasets(final int pNumDatasets) {
        this.numDatasets = pNumDatasets;
    }

    /**
     * @return the numInputs
     */
    public final int getNumInputs() {
        return this.numInputs;
    }

    /**
     * @param pNumInputs
     *            the numInputs to set
     */
    private void setNumInputs(final int pNumInputs) {
        this.numInputs = pNumInputs;
    }

    /**
     * @return the numOutputs
     */
    public final int getNumOutputs() {
        return this.numOutputs;
    }

    /**
     * @param pNumOutputs
     *            the numOutputs to set
     */
    private void setNumOutputs(final int pNumOutputs) {
        this.numOutputs = pNumOutputs;
    }

    /**
     * Adds values from the stacks to the output data file.
     */
    private void addRowDataFromStacks() {
        final float[] inData = new float[this.getNumInputs()];
        final float[] outData = new float[this.getNumOutputs()];

        for (int i = 0; i < this.inputDataStack.size(); i++) {
            inData[i] = this.inputDataStack.pop();
        }
        for (int j = 0; j < this.outputDataStack.size(); j++) {
            outData[j] = this.outputDataStack.pop();
        }
        this.addRowData(inData, outData);
    }

}
