/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.main;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.polarmsg.elize.base_classes.Engine;
import com.polarmsg.elize.interfaces.IEngine;
import com.polarmsg.elize.workers.BlackboardWorker;
import com.polarmsg.elize.workers.StartControllerWorker;
import com.polarmsg.elize.workers.Worker;

/**
 * Main class.
 * 
 * @author fredladeroute
 *
 */
public final class Main {

    /**
     * Private unused main constructor.
     */
    private Main() {
    }

    /**
     * Engine instance.
     */
    private static IEngine engine;

    /**
     * LinkedList of workers.
     */

    /**
     * Logger instance.
     */
    private static final Logger          LOGGER = Logger.getLogger(Main.class);


    /**
     * Main, where execution starts.
     * 
     * @param args
     *            List of arguments passed in through the command line
     */
    public static void main(final String[] args) {

        System.setProperty("freetts.voices",
                "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
        LinkedList<Worker> workers = new LinkedList<Worker>();
        workers.add(new BlackboardWorker());
        workers.add(new StartControllerWorker());
        engine = new Engine(workers);
        engine.start();
    }

    /**
     * Shutdown this thread.
     *
     * @return true if the shutdown succeeded
     */
    public static boolean shutdown() {
        engine.stop();
        System.exit(0);
        return true;
    }
}
