package com.polarmsg.elize.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;

/**
 * Image conversion utilities.
 *
 * @author fredladeroute
 *
 */
public final class ImageUtils {

    /**
     * Unused.
     */
    private ImageUtils() {

    }

    /**
     * Decode string to image.
     *
     * @param imageString
     *            The string to decode
     * 
     * @return decoded image
     */
    public static BufferedImage decodeToImage(final String imageString) {

        BufferedImage image = null;
        byte[] imageByte;
        try {
            imageByte = Base64.decodeBase64(imageString);
            final ByteArrayInputStream bis = new ByteArrayInputStream(
                    imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    /**
     * Encode image to string.
     *
     * @param image
     *            The image to encode
     * 
     * @param type
     *            jpeg, bmp, ...
     * 
     * @return encoded string
     */
    public static String encodeToString(final BufferedImage image,
            final String type) {
        String imageString = null;
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, type, bos);
            final byte[] imageBytes = bos.toByteArray();

            imageString = Base64.encodeBase64URLSafeString(imageBytes);

            bos.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        return imageString;
    }
}
