/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.polarmsg.elize.properties.PropertyManager;

/**
 * Database utility class.
 *
 * @author fredladeroute
 *
 */
public final class DatabaseUtils {

    private static DatabaseUtils instance;

    /**
     * Unused.
     */
    private DatabaseUtils() {
    }

    /**
     * Singleton constructor.
     * 
     * @return DatabaseUtils instance.
     */
    public static DatabaseUtils getInstance() {
        if (DatabaseUtils.instance == null) {
            return new DatabaseUtils();
        }
        return DatabaseUtils.instance;
    }

    /**
     * Location of the database file.
     */
    private static final String DATABASE_LOCATION = PropertyManager
            .getStringProperty("elize.core.blackboard.database.location",
                    "./database.db");

    /**
     * Logger instance.
     */
    private static final Logger LOGGER            = Logger
            .getLogger(DatabaseUtils.class);

    /**
     * Connection.
     */
    private Connection          connection;

    /**
     * Get a reference to the connection.
     *
     * @return a reference to the connection
     */
    public synchronized Connection getConnection() {
        try {
            if ((this.connection != null) && !this.connection.isClosed()) {
                return this.connection;
            } else {
                Class.forName("org.sqlite.JDBC");
                this.connection = DriverManager.getConnection(
                        "jdbc:sqlite:" + DatabaseUtils.DATABASE_LOCATION);
                connection.setAutoCommit(true);
            }
        } catch (final SQLException e) {
            e.printStackTrace();
            DatabaseUtils.LOGGER.error(e);
        } catch (final ClassNotFoundException e) {
            e.printStackTrace();
            DatabaseUtils.LOGGER.error(e);
        }
        return this.connection;
    }

    /**
     * Execute an SQL statement.
     *
     * @param stmt
     *            the statement to be executed.
     */
    public synchronized void executeStatement(final String stmt) {
        try {
            final Statement s = this.getConnection().createStatement();
            s.execute(stmt);
        } catch (final SQLException e) {
            e.printStackTrace();
            DatabaseUtils.LOGGER.error(e);
        }
    }

    /**
     * Execute an update on the database.
     *
     * @param stmt
     *            the update statement to be executed
     * 
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     *         statements or (2) 0 for SQL statements that return nothing or (3)
     *         if an SQLException is thrown
     */
    public synchronized int executeUpdate(final String stmt) {
        try {
            final Statement s = this.getConnection().createStatement();
            return s.executeUpdate(stmt);
        } catch (final SQLException e) {
            e.printStackTrace();
            DatabaseUtils.LOGGER.error(e);
            return -1;
        }
    }

    /**
     * Execute a query on the database.
     *
     * @param stmt
     *            the query statement to execute.
     * @return a ResultSet
     */
    public synchronized ResultSet executeQuery(final String stmt) {
        try {
            final Statement s = this.getConnection().createStatement();
            return s.executeQuery(stmt);
        } catch (final SQLException e) {
            e.printStackTrace();
            DatabaseUtils.LOGGER.error(e);
            return null;
        }
    }

    /**
     * Close the database connection.
     */
    public synchronized void dispose() {
        try {
            this.connection.close();
        } catch (final SQLException e) {
            DatabaseUtils.LOGGER.error(e);
        }
    }
}
