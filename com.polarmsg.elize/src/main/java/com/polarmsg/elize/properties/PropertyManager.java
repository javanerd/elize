/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * PropertyManager class used to get property values from the Property file
 * defined in FILE_NAME.
 * 
 * @author fredladeroute
 *
 */
public final class PropertyManager {

    /**
     * Logger instance.
     */
    private static final Logger     LOGGER    = Logger
            .getLogger(PropertyManager.class);

    /**
     * Property instance.
     */
    private static final Properties PROPS     = new Properties();

    /**
     * Property file name.
     */
    private static final String     FILE_NAME = "/opt/elize/config.properties";

    /**
     * True if the property file has been loaded into memory.
     */
    private static boolean          isLoaded  = false;

    static {
        PropertyManager.loadProperties();
    }

    /**
     * Private PropertyManager constructor, not used.
     */
    private PropertyManager() {
    }

    /**
     * Load properties into memory from file.
     */
    private static void loadProperties() {
        try {
            PropertyManager.PROPS
                    .load(new FileInputStream(PropertyManager.FILE_NAME));
            PropertyManager.LOGGER.debug("Properties loaded into memory.");
            PropertyManager.LOGGER
                    .debug(PropertyManager.PROPS.stringPropertyNames());
        } catch (final FileNotFoundException e) {
            PropertyManager.LOGGER.error(e);
            e.printStackTrace();
        } catch (final IOException e) {
            PropertyManager.LOGGER.error(e);
            e.printStackTrace();
        }
        PropertyManager.isLoaded = true;
    }

    /**
     * Write properties to file.
     */
    private static void writeProperties() {
        try {
            PropertyManager.PROPS.store(
                    new FileOutputStream(PropertyManager.FILE_NAME), null);
            PropertyManager.LOGGER.debug("Properties written to disk.");
        } catch (final FileNotFoundException e) {
            PropertyManager.LOGGER.error(e);
            e.printStackTrace();
        } catch (final IOException e) {
            PropertyManager.LOGGER.error(e);
            e.printStackTrace();
        }
    }

    /**
     * Add a property to this property manager.
     * 
     * @param key
     *            the key to store the property under
     * @param value
     *            the value of the property to be added
     * @param <T>
     *            the type of the value to store
     */
    public static <T extends Number> void addProperty(final String key,
            final T value) {
        if (!PropertyManager.isLoaded) {
            PropertyManager.loadProperties();
        }
        PropertyManager.PROPS.put(key, value);
        PropertyManager.writeProperties();
    }

    /**
     * Return a string property from the underlying PROPS instance.
     * 
     * @param key
     *            the key of the value to get
     * @return the value assigned the key key
     */
    public static String getStringProperty(final String key) {
        if (!PropertyManager.isLoaded) {
            PropertyManager.loadProperties();
        }
        if ((key == null) || (key.length() <= 0)) {
            throw new IllegalArgumentException("Key was null or zero length.");
        }
        return PropertyManager.PROPS.getProperty(key);
    }

    /**
     * Return a string property value.
     *
     * @param key
     *            the key of the value to get
     * 
     * @param defaultValue
     *            the default value to be returned if the key is not found
     * 
     * @return the value of key or default value if the key is not found
     */
    public static String getStringProperty(final String key,
            final String defaultValue) {
        String temp;
        if (!PropertyManager.isLoaded) {
            PropertyManager.loadProperties();
        }
        if ((key == null) || (key.length() <= 0)) {
            return defaultValue;
        }

        temp = PropertyManager.PROPS.getProperty(key);

        if ((temp == null) || (temp.length() <= 0)) {
            return defaultValue;
        }
        return temp;
    }

    /**
     * Get a property as a float.
     *
     * @param key
     *            the key to the value to be returned
     * 
     * @return the value with key key
     */
    public static float getFloatProperty(final String key) {
        return Float.parseFloat(PropertyManager.getStringProperty(key));
    }

    /**
     * Get a property as a float.
     *
     * @param key
     *            the key to the value to be returned
     * 
     * @param defaultValue
     *            the value to be returned if key is not found
     * 
     * @return the value with key key or if the key is not found return
     *         defaultValue
     */
    public static float getFloatProperty(final String key,
            final float defaultValue) {
        return Float.parseFloat(
                PropertyManager.getStringProperty(key, defaultValue + ""));
    }

    /**
     * Get a property as an Integer.
     *
     * @param key
     *            the key to the value to be returned
     * 
     * @return the value with key key
     */
    public static int getIntegerProperty(final String key) {
        return Integer.parseInt(PropertyManager.getStringProperty(key));
    }

    /**
     * Get a property as an Integer.
     *
     * @param key
     *            the key to the value to be returned
     * 
     * @param defaultValue
     *            the value to be returned if key is not found
     * 
     * @return the value with key key or if the key is not found return
     *         defaultValue
     */
    public static int getIntegerProperty(final String key,
            final int defaultValue) {
        return Integer.parseInt(
                PropertyManager.getStringProperty(key, defaultValue + ""));
    }

    /**
     * Get a property from the property as a long value.
     * 
     * @param key
     *            the key of the property to get
     * @return the value of the property
     */
    public static long getLongProperty(final String key) {
        return Long.parseLong(PropertyManager.getStringProperty(key));
    }

    /**
     * Get a property from the property as a long value. If the property cannot
     * be returned from the underlying PROPS instance then return defaultValue.
     *
     * @param key
     *            the key of the property to get
     * 
     * @param defaultValue
     *            the defaultValue to be returned if the value for key cannot be
     *            returned
     * @return the value for key or defaultValue if the value for key cannot be
     *         returned
     */
    public static long getLongProperty(final String key,
            final long defaultValue) {
        return Long.parseLong(
                PropertyManager.getStringProperty(key, defaultValue + ""));
    }

}
