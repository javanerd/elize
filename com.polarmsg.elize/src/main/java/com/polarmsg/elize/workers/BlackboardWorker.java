/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.workers;

import org.apache.log4j.Logger;

import com.polarmsg.elize.base_classes.Blackboard;
import com.polarmsg.elize.utils.DatabaseUtils;

/**
 * Initializes and starts the blackboard back-end.
 *
 * @author fredladeroute
 */
public final class BlackboardWorker extends Worker {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = Logger
            .getLogger(BlackboardWorker.class);

    /**
     * Blackboard.
     */
    private Blackboard          blackboard;

    /**
     * Blackboard contructor.
     */
    public BlackboardWorker() {
        super("Blackboard worker");
        BlackboardWorker.LOGGER.debug("Contructing BlackboardWorker.");
        this.setBlackboard(new Blackboard());
    }

    @Override
    public void run() {
        BlackboardWorker.LOGGER.debug("Bringing up datastore.");
        DatabaseUtils.getInstance().executeUpdate("BEGIN;"
                + "CREATE TABLE IF NOT EXISTS UserMessages("
                + "  idUserMessages "
                + "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "  sentence TEXT NOT NULL," + "  CONSTRAINT sentence_UNIQUE"
                + "    UNIQUE(sentence)" + ");"
                + "CREATE TABLE IF NOT EXISTS ReplyMessages("
                + "  idReplyMessages "
                + "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + "  sentence TEXT NOT NULL,"
                + "  UserMessages_idUserMessages INTEGER NOT NULL,"
                + "  CONSTRAINT ReplyMessagescol_UNIQUE"
                + "    UNIQUE(sentence),"
                + "  CONSTRAINT fk_ReplyMessages_UserMessages"
                + "    FOREIGN KEY(UserMessages_idUserMessages)"
                + "    REFERENCES UserMessages(idUserMessages)" + ");"
                + "CREATE INDEX IF NOT EXISTS "
                + "'fk_ReplyMessages_UserMessages_idx'"
                + " ON ReplyMessages(UserMessages_idUserMessages);"
                + "COMMIT;");
        BlackboardWorker.LOGGER.debug("Datastore operation complete.");
    }

    @Override
    public void dispose() {
        DatabaseUtils.getInstance().dispose();
    }

    /**
     * Return the blackboard instance variable.
     * 
     * @return the blackboard
     */
    public Blackboard getBlackboard() {
        return this.blackboard;
    }

    /**
     * Sets the Blackboard.
     *
     * @param b
     *            the blackboard to set
     */
    public void setBlackboard(final Blackboard b) {
        this.blackboard = b;
    }

}
