/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.workers;

import org.apache.log4j.Logger;

import com.polarmsg.elize.agents.CommandAgent;
import com.polarmsg.elize.agents.DataStorageAgent;
import com.polarmsg.elize.agents.DiscordAgent;
import com.polarmsg.elize.agents.InquiryAgent;
import com.polarmsg.elize.agents.NeuralNetAgent;
import com.polarmsg.elize.agents.ReplyAgent;
import com.polarmsg.elize.agents.StartupAgent;
import com.polarmsg.elize.agents.TrainerAgent;
import com.polarmsg.elize.base_classes.Controller;
import com.polarmsg.elize.base_classes.GlobalTimer;
import com.polarmsg.elize.properties.PropertyManager;

/**
 * A worker to kick off the controller process.
 * 
 * @author fredladeroute
 *
 */
public class StartControllerWorker extends Worker {

    /**
     * Single instance of the controller.
     */
    private volatile Controller  controller;

    /**
     * Global timer instance.
     */
    private volatile GlobalTimer globalTimer;

    /**
     * Logger instance.
     */
    private static final Logger  LOGGER                   = Logger
            .getLogger(StartControllerWorker.class);

    /**
     * The minimum allowable amount of time in milliseconds to sleep for the
     * GlobalTimer.
     */
    private static final long    MIN_SLEEP_TIME           = PropertyManager
            .getLongProperty("controller.min.sleeptime", 0L);

    /**
     * The maximum allowable sleep time for the GlobalTimer.
     */
    private static final long    MAX_SLEEP_TIME           = PropertyManager
            .getLongProperty("controller.max.sleeptime", 60000L);

    /**
     * The delay in milliseconds to wait before controller startup.
     */
    private static final long    INITIAL_CONTROLLER_DELAY = PropertyManager
            .getLongProperty("controller.initial.delay.millis", 10L);

    /**
     * The time to wait before execution of the agent in milliseconds.
     */
    private static final long    CONTROLLER_DELAY         = PropertyManager
            .getLongProperty("controller.period.millis", 1000L);

    /**
     * StartControllerWorker constructor.
     */
    public StartControllerWorker() {
        super("Start controller worker.");
    }

    @Override
    public final void run() {
        this.controller = new Controller();
        StartControllerWorker.LOGGER.debug("Starting controller.");
        this.controller.addAgent(new TrainerAgent());
        this.controller.addAgent(new DiscordAgent());
        this.controller.addAgent(new StartupAgent());
        this.controller.addAgent(new NeuralNetAgent());
        this.controller.addAgent(new DataStorageAgent());
        this.controller.addAgent(new ReplyAgent());
        this.controller.addAgent(new InquiryAgent());
        this.controller.addAgent(new CommandAgent());
        this.globalTimer = new GlobalTimer(StartControllerWorker.MAX_SLEEP_TIME,
                StartControllerWorker.MIN_SLEEP_TIME);
        StartControllerWorker.LOGGER
                .debug("Adding controller task to AgentTimer.");
        this.globalTimer.addTask(this.controller,
                StartControllerWorker.INITIAL_CONTROLLER_DELAY,
                StartControllerWorker.CONTROLLER_DELAY);
    }

    @Override
    public final void dispose() {
        this.controller.dispose();
        this.controller.cancel();
        this.globalTimer = null;
    }
}
