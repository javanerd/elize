/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.workers;

/**
 * Worker class.
 *
 * @author fredladeroute
 *
 */
public abstract class Worker implements Runnable {

    /**
     * The name of this work.
     */
    private String name;

    /**
     * Construct a new worker.
     *
     * @param n
     *            The name for this worker
     */
    public Worker(final String n) {
        this.setName(n);
    }

    /**
     * Gets the name of this worker.
     *
     * @return the name of this worker
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Sets the name of this worker.
     *
     * @param n
     *            the name to set
     */
    private void setName(final String n) {
        this.name = n;
    }

    /**
     * Dispose this working releasing all its resources.
     */
    public abstract void dispose();
}
