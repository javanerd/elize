/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.base_classes;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.polarmsg.elize.agents.AgentId;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.MessageType;

/**
 * Blackboard to store agent messages.
 * 
 * @author fredladeroute
 *
 */
public class Blackboard {

    /**
     * List of ACLMessages in memory.
     */
    private static List<ACLMessage> blackboard;

    /**
     * Logger instance.
     */
    private static final Logger     LOGGER = Logger.getLogger(Blackboard.class);

    /**
     * Blackboard constructor.
     */
    public Blackboard() {
        this.init();
    }

    /**
     * Searches the blackboard for the first occurrence of a message destined
     * for the AgentID id then removes it from the blackboard and returns the
     * message. If there is no message the method returns an empty message.
     *
     * @param id
     *            The agent id to search for.
     * @return The ACLMessage with the given id.
     */
    private static ACLMessage search(final AgentId id) {
        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
        for (int i = 0; i < Blackboard.getNumMessages(); ++i) {
            msg = Blackboard.blackboard.get(i);
            if (msg != null && msg.getReciever() == id) {
                Blackboard.blackboard.remove(i);
                return msg;
            }
        }
        return new ACLMessage(MessageType.EMPTY);
    }

    /**
     * Initializes this blackboard and sets up the in memory ACLMessage list.
     */
    public final void init() {
        Blackboard.blackboard = new ArrayList<ACLMessage>();
    }

    /**
     * Return the number of messages currently in the message list in memory.
     * 
     * @return the number of message currently in the blackboard.
     */
    public static int getNumMessages() {
        return Blackboard.blackboard.size();
    }

    /**
     * Adds an ACLMessage to the blackboard.
     * 
     * @param msg
     *            the message to be added to the blackboard.
     */
    public static void addMessage(final ACLMessage msg) {
        if ((msg != null) && msg.isValid()) {
            Blackboard.LOGGER
                    .debug("New ACLMessage, From: " + msg.getSender().name()
                            + " To: " + msg.getReciever().name() + " Action: "
                            + msg.getActionID().name());
            Blackboard.blackboard.add(msg);
        } else {
            Blackboard.LOGGER.debug("Could not validate ACLMessage.");
        }
    }

    /**
     * Searches and returns an ACLMessage destined for AgentID id.
     * 
     * @param id
     *            the AgentID to search the blackboard for.
     * @return the first message destined to agent with ID id.
     */
    public static ACLMessage getMessage(final AgentId id) {
        return Blackboard.search(id);
    }

}
