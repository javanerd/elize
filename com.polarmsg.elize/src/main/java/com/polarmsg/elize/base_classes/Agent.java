/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.base_classes;

import java.util.LinkedList;

import com.polarmsg.elize.agents.AgentId;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;

/**
 * Agent an abstract base class.
 * 
 * @author fredladeroute
 *
 */
public abstract class Agent {

    /**
     * True if this agent is enabled and running.
     */
    private boolean                      enabled;

    /**
     * Internal messaging queue.
     */
    private final LinkedList<ACLMessage> messageQueue;

    /**
     * The name of this agent.
     */
    private String                       name;

    /**
     * Unique id to this agent. No two agents will have the same id.
     */
    private AgentId                      id;

    /**
     * Agent constructor.
     * 
     * @param iname
     *            the name of the agent to construct
     * @param idx
     *            the id of the agent to construct
     */
    public Agent(final String iname, final AgentId idx) {
        this.setName(iname);
        this.messageQueue = new LinkedList<ACLMessage>();
        this.enable();
        this.setId(idx);
    }

    /**
     * Activate this agent called once on agent creation.
     */
    public abstract void activate();

    /**
     * Agent update called in sequence, all agents are updated sequentially
     * order matters.
     */
    public abstract void update();

    /**
     * Dispose, called on the destruction of the agent. Use to clean up
     * resources.
     */
    public abstract void dispose();

    /**
     * Checks the blackboard for a message addressed to this agent and adds it
     * to its' message queue.
     * 
     * @return true if a message is found and was successfully added to message
     *         queue.
     */
    public final boolean checkBlackboard() {
        ACLMessage msg = new ACLMessage(MessageType.EMPTY);
        if (Blackboard.getNumMessages() > 0) {
            msg = Blackboard.getMessage(this.getId());
            if (msg != null) {
                return this.messageQueue.add(msg);
            }
        }

        return false;
    }

    /**
     * Return the next message on the message queue.
     * 
     * @return the next message on the message queue
     */
    protected final ACLMessage getNextMessage() {
        return this.messageQueue.poll();
    }

    /**
     * Return the name of this agent.
     * 
     * @return the name of this agent
     */
    public final String getName() {
        return this.name;
    }

    /**
     * @param iname
     *            the name to set
     */
    private void setName(final String iname) {
        this.name = iname;
    }

    /**
     * Returns true if this agent is enabled.
     * 
     * @return true if this agent is enabled.
     */
    public final boolean enabled() {
        return this.enabled;
    }

    /**
     * Disables this agent and sets Agent.enabled() to false.
     */
    protected final void disable() {
        this.enabled = false;
    }

    /**
     * Enable this agent and sets Agent.enabled() to true.
     */
    protected final void enable() {
        this.enabled = true;
    }

    /**
     * Returns the id of this agent.
     * 
     * @return the id of this agent
     */
    public final AgentId getId() {
        return this.id;
    }

    /**
     * Sets the id of this agent.
     * 
     * @param idx
     *            the id to set
     */
    private void setId(final AgentId idx) {
        this.id = idx;
    }

    /**
     * Send a message to another agent using ACLMessages.
     *
     * @param type
     *            The type of message to send.
     * @param action
     *            The action to be performed.
     * @param to
     *            The destination agent
     * @param content
     *            The content of the message (this value can be null or empty).
     */
    protected final void send(final MessageType type, final ActionId action,
            final AgentId to, final String content) {
        final ACLMessage msg = new ACLMessage(type);
        msg.setActionID(action);
        msg.setReciever(to);
        msg.setSender(this.getId());
        msg.setContent(content);
        msg.postMessage();
    }
}
