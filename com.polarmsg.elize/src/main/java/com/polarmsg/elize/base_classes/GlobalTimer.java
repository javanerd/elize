/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.base_classes;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

/**
 * Global timer class used by the controller to schedule agent updates.
 * 
 * @author fredladeroute
 *
 */
public class GlobalTimer {

    /**
     * Timer instance.
     */
    private Timer               timer;

    /**
     * Max sleep time.
     */
    private long                maxSleeptime;

    /**
     * Min sleep time.
     */
    private long                minSleeptime;

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = Logger.getLogger(GlobalTimer.class);

    /**
     * GlobalTimer constructor.
     * 
     * @param pmaxSleeptime
     *            the max sleep time between task updates.
     * @param pminSleeptime
     *            the min sleep time between task updates.
     */
    public GlobalTimer(final long pmaxSleeptime, final long pminSleeptime) {
        this.setTimer(new Timer("AgentTimer"));
        this.setMaxSleeptime(pmaxSleeptime);
        this.setMinSleeptime(pminSleeptime);
        GlobalTimer.LOGGER.debug("Starting AgentTimer.");
    }

    /**
     * Add a task to the timertask queue.
     * 
     * @param task
     *            the task to be scheduled
     * @param delay
     *            the delay in milliseconds before the task is to be executed
     * @param period
     *            time in milliseconds between successive task executions
     */
    public final void addTask(final TimerTask task, final long delay,
            final long period) {
        if (task == null) {
            throw new IllegalArgumentException(
                    "Error task to be executed was null.");
        } else if ((period > this.maxSleeptime)
                || (period < this.minSleeptime)) {
            task.cancel();
            GlobalTimer.LOGGER
                    .debug("Period out of bounds. Cancelling. " + period);
        } else {
            GlobalTimer.LOGGER.debug("GlobalTimer started. Period: " + period);
            this.getTimer().scheduleAtFixedRate(task, delay, period);
        }
    }

    /**
     * Get the timer.
     * 
     * @return the timer
     */
    public final Timer getTimer() {
        return this.timer;
    }

    /**
     * Set the timer.
     * 
     * @param ptimer
     *            the timer to set
     */
    private void setTimer(final Timer ptimer) {
        this.timer = ptimer;
    }

    /**
     * Get the max sleep time.
     * 
     * @return the max sleep time acceptible for the global timer period
     */
    public final long getMaxSleeptime() {
        return this.maxSleeptime;
    }

    /**
     * Set the max sleep time.
     * 
     * @param pmaxSleeptime
     *            the max sleep time acceptable for the global timer period
     */
    private void setMaxSleeptime(final long pmaxSleeptime) {
        this.maxSleeptime = pmaxSleeptime;
    }

    /**
     * Get the min sleep time.
     * 
     * @return the min sleep time
     */
    public final long getMinSleeptime() {
        return this.minSleeptime;
    }

    /**
     * Set the min sleep time.
     * 
     * @param pminSleeptime
     *            the min sleep time
     */
    private void setMinSleeptime(final long pminSleeptime) {
        this.minSleeptime = pminSleeptime;
    }

}
