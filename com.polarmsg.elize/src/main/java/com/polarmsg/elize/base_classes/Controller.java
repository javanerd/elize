/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.base_classes;

import java.util.ArrayList;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.polarmsg.elize.agents.AgentId;

/**
 * Controller this controller co-ordinates execution of agents.
 * 
 * @author fredladeroute
 *
 */
public class Controller extends TimerTask {

    /**
     * List of agents in this controller.
     */
    private final ArrayList<Agent> agents;

    /**
     * True if the controller has been initialized.
     */
    private boolean                initialized;

    /**
     * Logger instance.
     */
    private static final Logger    LOGGER = Logger.getLogger(Controller.class);

    /**
     * Controller constructor.
     */
    public Controller() {
        this.agents = new ArrayList<Agent>();
    }

    /**
     * Adds an agent to the internal list of agents to be run.
     *
     * @param a
     *            the agent to be added to this controller
     */
    public final void addAgent(final Agent a) {
        if (a != null) {
            Controller.LOGGER
                    .debug("Adding agent to controller: " + a.getName());
            this.agents.add(a);
        }
        if (this.initialized) {
            a.activate();
        }
    }

    /**
     * Removes an agent by ID.
     *
     * @param id
     *            the id of the agent to be removed
     */
    public final void removeAgent(final AgentId id) {
        if ((AgentId.getId(id) >= 0)
                && (AgentId.getId(id) < this.agents.size())) {
            Controller.LOGGER.debug("Removing agent from controller: "
                    + this.agents.get(AgentId.getId(id)).getName());
            this.agents.remove(AgentId.getId(id));
        }
    }

    /**
     * Initialize all agents within this controller and call their
     * Agent.activate() method.
     */
    private void initialize() {
        Controller.LOGGER.debug("Initializing Controller.");
        for (int index = 0; index < this.agents.size(); index++) {
            Controller.LOGGER.debug(
                    "Activating agent: " + this.agents.get(index).getName());
            this.agents.get(index).activate();
        }
        this.initialized = true;
    }

    @Override
    public final void run() {
        if (!this.initialized) {
            this.initialize();
        }
        for (int index = 0; index < this.agents.size(); index++) {
            if (this.agents.get(index).enabled()) {
                this.agents.get(index).update();
            }
        }
    }

    /**
     * Dispose the controller and all it's agents.
     */
    public final void dispose() {
        for (int index = 0; index < this.agents.size(); index++) {
            if (this.agents.get(index).enabled()) {
                this.agents.get(index).dispose();
            }
        }
    }

}
