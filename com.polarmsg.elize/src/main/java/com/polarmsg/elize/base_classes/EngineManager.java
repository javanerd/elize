/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.base_classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;

import com.polarmsg.elize.interfaces.IEngine;

/**
 * EngineManager is responsible for keeping track of engines.
 * 
 * @author fredladeroute
 *
 */
public class EngineManager {

    /**
     * Stack of engines.
     */
    private static Stack<IEngine> engineStack = new Stack<IEngine>();

    /**
     * List of engine references.
     */
    private List<IEngine>         engineReferences;

    /**
     * Logger instance.
     */
    public static final Logger    LOGGER      = Logger
            .getLogger(EngineManager.class);

    /**
     * EngineManager constructor.
     * 
     * @param engines
     *            array of engines to be added to this engine upon start
     */
    public EngineManager(final IEngine... engines) {
        this();
        for (final IEngine i : engines) {
            this.addEngine(i);
        }
    }

    /**
     * Default constructor.
     */
    public EngineManager() {
        this.setEngineReferences(new ArrayList<IEngine>());
    }

    /**
     * Start the engines in this engine manager.
     */
    public final void startEngines() {
        EngineManager.LOGGER.debug("EngineManager starting...");
        while (!EngineManager.engineStack.empty()) {
            EngineManager.engineStack.pop().start();
        }
    }

    /**
     * Add an engine to this manager.
     *
     * @param i
     *            the IEngine to be added
     */
    public final void addEngine(final IEngine i) {

        if (i == null) {
            throw new IllegalArgumentException("Engine was null, "
                    + "while attempting to add it to the EngineFactory.");
        }
        EngineManager.LOGGER.debug("Adding an Engine to EngineFactory");
        EngineManager.engineStack.push(i);
        this.getEngineReferences().add(i);
    }

    /**
     * Get an engine from this engine manager.
     *
     * @param index
     *            the index of the engine to get
     * @return the IEngine at index index
     *
     */
    public final IEngine getEngine(final int index) {
        return this.getEngineReferences().get(index);
    }

    /**
     * Stop all engines managed by this engine manager.
     */
    public final void stopEngines() {
        EngineManager.LOGGER.debug("Attempting to stop via EngineManager...");
        for (final IEngine i : this.getEngineReferences()) {
            i.stop();
        }
    }

    /**
     * Get engine references.
     * 
     * @return the engineReferences
     */
    private List<IEngine> getEngineReferences() {
        return this.engineReferences;
    }

    /**
     * Set engine references.
     *
     * @param pEngineReferences
     *            the engineReferences to set
     */
    private void setEngineReferences(final List<IEngine> pEngineReferences) {
        this.engineReferences = pEngineReferences;
    }

}
