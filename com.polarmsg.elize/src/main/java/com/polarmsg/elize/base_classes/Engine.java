/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.base_classes;

import java.util.LinkedList;

import org.apache.log4j.Logger;

import com.polarmsg.elize.interfaces.IEngine;
import com.polarmsg.elize.workers.Worker;

/**
 * Engine class used at the core of brain-bot to manage the controller.
 * 
 * @author fredladeroute
 *
 */
public class Engine implements IEngine {

    /**
     * Return true if the Engine is running.
     */
    private boolean             isRunning = false;

    /**
     * Logger instance.
     */
    private static final Logger LOGGER    = Logger.getLogger(Engine.class);

    /**
     * LinkedList of tasks.
     */
    private LinkedList<Worker>  tasks;

    /**
     * Engine constructor.
     * 
     * @param taskQueue
     *            list of tasks to be run by this engine
     */
    public Engine(final LinkedList<Worker> taskQueue) {
        this.setTasks(taskQueue);
    }

    @Override
    public final void start() {
        if (!this.isRunning) {
            this.isRunning = true;
            Engine.LOGGER.debug("Engine is starting...");
            for (final Worker w : this.tasks) {
                w.run();
            }
            Engine.LOGGER.debug("Engine is started.");
        }
    }

    @Override
    public final void stop() {
        if (this.isRunning) {
            Engine.LOGGER.debug("Engine is stopping...");
            for (final Worker w : this.tasks) {
                w.dispose();
            }
            Engine.LOGGER.debug("Engine is stopped.");
            this.isRunning = false;
        }
    }

    @Override
    public final void restart() {
        this.stop();
        this.start();
    }

    /**
     * Get the list of tasks.
     * 
     * @return the LinkedList of tasks
     */
    public final LinkedList<Worker> getTasks() {
        return this.tasks;
    }

    /**
     * @param taskQueue
     *            the tasks to set
     */
    private void setTasks(final LinkedList<Worker> taskQueue) {
        this.tasks = taskQueue;
    }

    @Override
    public final boolean isRunning() {
        return this.isRunning;
    }
}
