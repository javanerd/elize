/******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.gui;

import java.net.URL;
import java.util.ResourceBundle;

import com.polarmsg.elize.agents.AgentId;
import com.polarmsg.elize.main.Main;
import com.polarmsg.elize.messages.ACLMessage;
import com.polarmsg.elize.messages.ActionId;
import com.polarmsg.elize.messages.MessageType;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

/**
 * GuiController responsible for interfacing with JavaFX GUI.
 *
 * @author fred laderoute
 *
 */
public class GuiController implements Initializable {

    /**
     * Border Pane.
     */
    @FXML
    private BorderPane borderPane;

    /**
     * Exit Button.
     */
    @FXML
    private Button     exitButon;

    /**
     * HBox.
     */
    @FXML
    private HBox       hBox;

    /**
     * Root Pane.
     */
    @FXML
    private AnchorPane rootPane;

    /**
     * Scroll Pane.
     */
    @FXML
    private ScrollPane scrollPane;

    /**
     * Send Button.
     */
    @FXML
    private Button     sendButton;

    /**
     * Text Area.
     */
    @FXML
    private TextArea   textArea;

    /**
     * Text Field.
     */
    @FXML
    private TextField  textField;

    /**
     * Tool bar.
     */
    @FXML
    private ToolBar    toolBar;

    /**
     * GUI Controller Constructor.
     */
    public GuiController() {
    }

    @Override
    public final void initialize(final URL fxmlFileLocation,
            final ResourceBundle resources) {
        assert this.borderPane != null : "fx:id=\"borderPane\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.exitButon != null : "fx:id=\"exitButon\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.hBox != null : "fx:id=\"hBox\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.rootPane != null : "fx:id=\"rootPane\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.scrollPane != null : "fx:id=\"scrollPane\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.sendButton != null : "fx:id=\"sendButton\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.textArea != null : "fx:id=\"textArea\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.textField != null : "fx:id=\"textField\" was not injected: check your FXML file 'gui.fxml'.";
        assert this.toolBar != null : "fx:id=\"toolBar\" was not injected: check your FXML file 'gui.fxml'.";
    }

    /**
     * Used by GuiApplication to shutdown system on gui termination.
     *
     * @return true if shutdown is allowed.
     */
    public final boolean shutdown() {
        return Main.shutdown();
    }

    /**
     * Send button event handler.
     *
     * @param event
     *            the event passed in from the GUI
     */
    @FXML
    protected final void handleSendButtonAction(final ActionEvent event) {
        final ACLMessage msg = new ACLMessage(MessageType.SEND,
                ActionId.RAWDATA);
        msg.setContent(this.textField.getText());
        this.textField.setText("");
        msg.setSender(AgentId.DISCORD);
        msg.setReciever(AgentId.DATA_STORAGE);
        msg.postMessage();
        this.textArea.appendText(msg + System.lineSeparator());
    }

    /**
     * Prints a message to the message box.
     *
     * @param msg
     *            the message to print to the gui
     */
    public final void print(final String msg) {
        if ((msg != null) && (msg.length() > 0)) {
            this.textArea.appendText(msg + System.lineSeparator());
        }
    }

}
