/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.gui;

import java.io.IOException;

import com.polarmsg.elize.properties.PropertyManager;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author fredladeroute
 *
 */
public class GuiApplication extends Application {

    /**
     * Gui controller instance.
     */
    private final GuiController guiCtrl;

    /**
     * GUI parent.
     */
    private Parent              root;

    /**
     * Gui Application Constructor.
     */
    public GuiApplication() {
        final FXMLLoader ldr = new FXMLLoader(
                this.getClass().getResource("/gui.fxml"));
        this.root = null;
        try {
            this.root = (Parent) ldr.load();
        } catch (final IOException e) {
            e.printStackTrace();
        }
        this.guiCtrl = ldr.getController();
    }

    /**
     * Initial width of this GUI in pixels.
     */
    private static final int    WIDTH  = PropertyManager
            .getIntegerProperty("elize.core.gui.width", 300);

    /**
     * Initial height of this GUI in pixels.
     */
    private static final int    HEIGHT = PropertyManager
            .getIntegerProperty("elize.core.gui.height", 275);

    /**
     * GUI title.
     */
    private static final String TITLE  = PropertyManager
            .getStringProperty("elize.core.gui.title", "Elize");

    @Override
    public final void start(final Stage stage) throws Exception {

        final Scene scene = new Scene(this.root, GuiApplication.WIDTH,
                GuiApplication.HEIGHT);
        stage.setTitle(GuiApplication.TITLE);
        stage.setScene(scene);
        stage.show();
        scene.getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(final WindowEvent ev) {
                if (!GuiApplication.this.guiCtrl.shutdown()) {
                    ev.consume();
                }
            }
        });
    }

    /**
     * Called by GuiAgent to update the gui.
     *
     * @param msg
     *            The message recieved from the GuiAgent.
     */
    public final void update(final String msg) {
        this.guiCtrl.print(msg);
    }
}
