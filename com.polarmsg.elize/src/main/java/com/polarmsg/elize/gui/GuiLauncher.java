/*******************************************************************************
 * Copyright (c) 2015 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.gui;

import javafx.application.Application;
import javafx.application.Platform;

/**
 * Gui Launcher for launching GUI.
 * 
 * @author fredladeroute
 *
 */
public class GuiLauncher implements Runnable {

    @Override
    public final void run() {
        Application.launch(GuiApplication.class);
    }

    /**
     * Release resources and clean up.
     */
    public final void dispose() {
        Platform.exit();
    }

}
