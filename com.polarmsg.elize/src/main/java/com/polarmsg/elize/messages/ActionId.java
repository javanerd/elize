/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.messages;

/**
 * Represents an ActionID for an ACL message.
 * 
 * @author fredladeroute
 *
 */
public enum ActionId {

    /**
     * Null action ID.
     */
    NULL(0),

    /**
     * Train action id.
     *
     * Used to instruct the Training agent to train on the data given in the
     * message content.
     */
    TRAIN(1),

    /**
     * Delete records from the database.
     */
    DELETE(2),

    /**
     * The content is composed of raw ("new") data.
     */
    RAWDATA(3),

    /**
     * Print the content of the message.
     */
    PRINT(4),

    /**
     * Speak the contents of the message.
     */
    SPEAK(5),

    /**
     * Ask the user for some input.
     */
    INQUIRE(6),

    /**
     * Reply to the sender agent.
     */
    REPLY(7),

    /**
     * Inform action, general purpose action ID used to inform an agent about
     * some new data.
     */
    INFORM(8),

    /**
     * Check if the content corrisonds to a command message.
     */
    CHECK_CMD(9),

    /**
     * No action is performed on the contents of the message.
     */
    NO_ACTION(10);

    /**
     * The internal ID field.
     */
    private int id;

    /**
     * Private ActionID constructor.
     * 
     * @param i
     *            the id of the enum type.
     */
    private ActionId(final int i) {
        this.id = i;
    }

    /**
     * Return the id of the given ActionID.
     *
     * @param t
     *            the ActionID to get the id of
     * 
     * @return the ordinal value of the ActionID t
     */
    public static int getId(final ActionId t) {
        return t.id;
    }
}
