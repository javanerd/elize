/*******************************************************************************
 * Copyright (c) 2013 Fred Laderoute.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the GNU Public License v3.0
 * which accompanies this distribution, and is available at
 * http://www.gnu.org/licenses/gpl.html
 *
 * Contributors:
 *     Fred Laderoute - initial API and implementation
 ******************************************************************************/
package com.polarmsg.elize.messages;

import com.polarmsg.elize.agents.AgentId;
import com.polarmsg.elize.base_classes.Blackboard;

/**
 * Agent Communication Language Message.
 *
 * @author fredladeroute
 */

public final class ACLMessage {

    /**
     * Message type for this ACL Message.
     */
    private MessageType messageType;

    /**
     * The action id for this ACL Message.
     */
    private ActionId    actionId;

    /**
     * The contents of the message.
     */
    private String      content;

    /**
     * The sender of the ACL Message.
     */
    private AgentId     sender;

    /**
     * The intended recipient of the ACL Message.
     */
    private AgentId     reciever;

    /**
     * True if this message is empty.
     */
    private boolean     isEmpty = false;

    /**
     * Contruct a new ACL message given a message type and an ActionID.
     *
     * @param type
     *            the MessageType which defines the message type of this message
     * 
     * @param id
     *            the ActionID of this message
     */
    public ACLMessage(final MessageType type, final ActionId id) {
        if (type == MessageType.EMPTY) {
            this.isEmpty = true;
        }
        this.setId(type);
        this.setActionID(id);
    }

    /**
     * Constructs a new Null ACL message, where the MessageType is id and the
     * ActionID is null.
     *
     * @param id
     *            the MessageType for this ACL message
     */
    public ACLMessage(final MessageType id) {
        this(id, ActionId.NULL);
    }

    /**
     * Returns true if the message is empty and therefore has no content.
     *
     * @return true if the message is empty
     */
    public boolean isEmpty() {
        return this.isEmpty;
    }

    /**
     * Set the ActionID for this Message.
     * 
     * @param id
     *            the ActionID to set.
     */
    public void setActionID(final ActionId id) {
        this.actionId = id;
    }

    /**
     * Returns this ACLMessages ActionID.
     * 
     * @return the ActionID for this ACLMessage
     */
    public ActionId getActionID() {
        return this.actionId;
    }

    /**
     * Sets the MessageType for this ACL Message.
     * 
     * @param id
     *            the MessageType to set
     */
    private void setId(final MessageType id) {
        this.messageType = id;
    }

    /**
     * Set the contents of this message.
     * 
     * @param c
     *            the content to be set
     */
    public void setContent(final String c) {
        this.content = c;
    }

    /**
     * Returns the contents of this message.
     * 
     * @return the contents of this message
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Sets the sender of this message.
     * 
     * @param id
     *            the senders AgentID to be set
     */
    public void setSender(final AgentId id) {
        this.sender = id;
    }

    /**
     * Returns the sender of this ACL message.
     * 
     * @return the sender of this ACL message
     */
    public AgentId getSender() {
        return this.sender;
    }

    /**
     * Returns the expected receiver of this ACL message.
     * 
     * @return the expected receiver of this ACL message
     */
    public AgentId getReciever() {
        return this.reciever;
    }

    /**
     * Set the reciever of this ACL message.
     * 
     * @param id
     *            the id of the reciever to be set
     */
    public void setReciever(final AgentId id) {
        this.reciever = id;
    }

    /**
     * Return the message type of this ACL message.
     * 
     * @return the message type of this ACL message.
     */
    public MessageType getMessageType() {
        return this.messageType;
    }

    @Override
    public String toString() {
        return "{" + this.messageType.name() + "," + this.sender.name() + ","
                + this.reciever.name() + "," + this.content + "}";
    }

    /**
     * Post this ACL message to the Blackboard.
     *
     * @see Blackboard#addMessage(ACLMessage)
     */
    public void postMessage() {
        if (this.isValid()) {
            Blackboard.addMessage(this);
        }
    }

    /**
     * Validate this ACL message to ensure that it can be sent to the
     * blackboard.
     *
     * @return true if this message is valid.
     */
    public boolean isValid() {
        if ((this.messageType != null) && (this.sender != null)
                && (this.reciever != null) && (this.actionId != null)) {
            return true;
        }
        return false;
    }

}
