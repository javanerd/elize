Elize
=========

**How to get setup.**

1. Install Oracle JDK.
2. Install Eclipse JavaEE edition.
3. Setup eclipse to use Oracle Java.
4. Install Maven.
5. Install e(fx)clipse plugin.
6. run "./setup -r"
7. Import new maven project, select com.polarmsg.elize